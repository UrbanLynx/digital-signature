﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Digital_Signature
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string fromFile;
        private byte[] signature;
        private DSACryptoServiceProvider DSA = new DSACryptoServiceProvider();

        private void button3_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                fromFile = dlg.FileName;
                textBox1.Text = fromFile;
            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            var hash = CalculateHash(fromFile);

            var DSAFormatter = new DSASignatureFormatter(DSA);

            DSAFormatter.SetHashAlgorithm("SHA1");
            signature = DSAFormatter.CreateSignature(hash);
        }

        private byte[] CalculateHash(string filename)
        {
            var data = File.ReadAllBytes(filename);
            var sha = new SHA1CryptoServiceProvider();
            var hash = sha.ComputeHash(data);
            return hash;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var hash = CalculateHash(fromFile);

            var DSADeformatter = new DSASignatureDeformatter(DSA);

            DSADeformatter.SetHashAlgorithm("SHA1");
            if (DSADeformatter.VerifySignature(hash, signature))
            {
                MessageBox.Show("The signature was verified.");
            }
            else
            {
                MessageBox.Show("The signature was not verified.");
            }
        }


    }
}
